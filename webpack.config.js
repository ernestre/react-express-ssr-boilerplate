const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');

module.exports = [
    {
        entry: './src/client/index.js',
        mode: 'production',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'app.js',
            publicPath: '/'
        },
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['babel-preset-env', 'babel-preset-react']
                        }
                    }
                }
            ]
        }
    },
    {
        entry: './src/server.js',
        mode: 'production',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'server.js',
            publicPath: '/'
        },
        target: 'node',
        externals: nodeExternals(),
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['babel-preset-env', 'babel-preset-react']
                        }
                    }
                }
            ]
        }
    }
]

