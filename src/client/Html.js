const Html = ({ body, title }) => `
  <!DOCTYPE html>
  <html>
    <head>
      <title>${title}</title>
    </head>
    <body>
      <div id="app">${body}</div>
    </body>
    <script src="/assets/app.js"></script>
  </html>
`;

export default Html;
