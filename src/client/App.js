import React from 'react';

class App extends React.Component {

    constructor() {
        super()

        this.state = {
            count: 0,
        }

        this.bumpCount = this.bumpCount.bind(this);
    }


    bumpCount() {
        this.setState({ count: this.state.count + 1 })
    }

    render() {
        return (
            <div>
                <p>
                    count: { this.state.count } 💅
                </p>
                <button onClick={ this.bumpCount }>
                    bump
                </button>
            </div>
        )
    }
}

export default App;
