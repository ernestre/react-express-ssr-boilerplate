import express from 'express';
import compression from 'compression'
import React from 'react';
import { renderToString } from 'react-dom/server';
import Html from './client/Html.js'
import App from './client/App.js'

const app = express();

app.use('/assets', express.static('dist'))
app.use(compression());
app.disable('x-powered-by');

app.get('/', (req, res) => {

    const app  = renderToString(<App />);

    res.send(Html({
        'body': app,
        'title': 'title'
    }));
});

export default app;
